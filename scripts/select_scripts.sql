-- Наименования товара и его цена, если на складе больше 50 штук
select name, actual_price
from goods
where actual_amount > 50
order by actual_amount desc

-- В составе есть полиэстр
select goods.article, name
from composition
join goods on composition.article = goods.article
where material =  'Полиэстр'
group by material, goods.article

-- Поставщики, которые совершили больше одной поставки
select providerid, count(amount) as amount_supplies
from supply_history
group by providerid
having count(providerid) > 1

-- Все товары, у которых в составе меньше 25% полиэстра
select composition.article, name
from composition
join goods on goods.article = composition.article
where composition.article not in (
    select article
    from composition
    where material = 'Полиэстр' and percentage >= 25
    )
group by composition.article, name

-- Количество различных видов поставляемого товара для всех поставщиков
select providerID, count(*)
from provider
group by providerID
order by providerID

-- Суммарная цена проданных товаров, которые не вернули
select sum(purchase_price) over () - sum(case when returned = true then purchase_price else 0 end) over () as total_earn
from purchases_history
order by purchase_price
 desc
limit 1;

-- Покупки, сгруппированные по дате (т.е. каждой строке соответсвует один чек)
select dense_rank() over (order by date) as n, count(*) as amount_goods, sum(purchase_price) as sum
from purchases_history
group by date
order by date


-- Товары, содержащие в своем составе count материалов с процентом от 15 до 50
select article, count(*) over (partition by material rows between unbounded preceding and unbounded following),  percentage
from composition
where percentage between 15 and 50
order by article


-- Ранжирует поставщиков по количеству поставляемого товара
with N_supplies as (
    select providerID, count(*) as cnt
    from provider
    group by providerID
)

select providerid, dense_rank() over (order by cnt desc) as rank, cnt
from N_supplies


-- Количество возвратов за последние 5 покупок (включая текущую)
with cnt as (select purchaseid,
                    case
                        when returned = true then
                                    count(*)
                                    over (partition by returned order by purchaseID rows between 4 preceding and current row)
                        else 0
                        end as ret
             from purchases_history
             order by purchaseID)

select purchaseID, max(ret) over (order by purchaseID rows between 4 preceding and current row)
from cnt;