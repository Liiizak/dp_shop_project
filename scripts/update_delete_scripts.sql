update goods
set article = 87532849
where article = 53497520;

update composition
set percentage = 45
where article = 55795382 and material = 'Кожа искусственная';

-- Добавление товара в актуальное количество, который поступил с 1 января
with changes as (
    select case when sum(amount) is not null then sum(amount) else 0 end as change, goods.article
from supply_history
right join goods on goods.article = supply_history.article
where date::date >= date '2023-01-01' or date is null
group by goods.article
)

update goods
set actual_amount = actual_amount + (
    select change
    from changes
    where changes.article = goods.article);

delete from purchases_history
where purchaseID = 10;

update provider
set providerID = 8
where providerid = 5;