create table goods (
  name text not null,
  article integer primary key,
  actual_amount integer,
  actual_price real
);

create table provider (
    providerID serial,
    article integer,
    primary key(providerID, article),
    foreign key(article) references goods(article) on delete cascade on update restrict
);

create table purchases_history (
  purchaseID serial primary key,
  date timestamp,
  article integer,
  purchase_price real,
  returned bool,
  foreign key(article) references goods(article) on delete no action on update restrict
);

create table composition (
  material text not null,
  article integer,
  percentage real not null,
  primary key(material, article),
  foreign key(article) references goods(article) on delete cascade on update restrict
);

create table supply_history (
  supplyID serial primary key,
  providerID serial,
  date timestamp,
  article integer,
  amount integer not null,
  foreign key(article) references goods(article) on delete no action on update restrict,
  foreign key(providerID, article) references provider(providerID, article) on delete no action on update restrict
);